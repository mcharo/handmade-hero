@echo off

REM if 'cl' is in the PATH go straight to buildling
REM otherwise, call vcvarsall to set up env
where.exe cl >nul 2>&1
IF %ERRORLEVEL% EQU 0 GOTO build
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

:build
REM ~dp0 is the folder of _this_ file with trailing slash
mkdir "%~dp0..\build" 2>nul
pushd "%~dp0..\build"
cl -DHANDMADE_WIN32=1 -FC -Zi "%~dp0win32_handmade.cpp" user32.lib gdi32.lib
popd