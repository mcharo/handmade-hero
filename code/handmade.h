#if !defined(HANDMADE_H)


// FOUR THINGS - timing, controller/keyboard input, bitmap buffer to use, sound buffer to use, 

struct game_offscreen_buffer
{
    void *Memory;
    int Width;
    int Height;
    int Pitch;
};

internal void GameUpdateAndRender(game_offscreen_buffer *Buffer, int BlueOffset, int GreenOffset);

#define HANDMADE_H
#endif