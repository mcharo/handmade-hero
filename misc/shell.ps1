# dot-source this file from $env:USERPROFILE\Documents\WindowsPowerShell\profile.ps1

#region load visual studio environment variables
# because the vcvarsall.bat can't set environment variables directly in powershell
# run vcvarsall and then `set` to output environment variables and then we'll parse
# them and set them in the PowerShell session
$VCVARSALL = Get-ChildItem -Path "C:\Program Files (x86)" -Filter vcvarsall.bat -Recurse -ErrorAction SilentlyContinue | Select-Object -First 1
if ($VCVARSALL)
{
    $Command = '"{0}" x64 & set' -f $VCVARSALL.FullName
    $CmdEnvs = cmd /c $Command
    foreach ($CmdEnv in $CmdEnvs)
    {
        if ($CmdEnv -match "=")
        {
            $k, $v = $CmdEnv.split("=")
            Set-Item -Force -Path "ENV:\$k"  -Value "$v"
        }
    }
}
else
{
    Write-Host "Unable to find vcvarsall.bat"
}
#endregion

#region map workspace drive
$Documents = [System.Environment]::GetFolderPath('MyDocuments')
if (Test-Path -Path $Documents\workspace)
{
    if ($null -eq (Get-PSDrive -Name W -ErrorAction SilentlyContinue))
    {
        $null = New-PSDrive -Name W -PSProvider FileSystem -Root $Documents\workspace
    }
    Set-Location -Path W:
}
if (Test-Path -Path W:\handmade)
{
    Set-Location -Path W:\handmade
}
#endregion

#region shell functions
function New-Folder
{
    [CmdletBinding()]
    param(
        $Path,
        [switch]$SetLocation
    )
    if (-Not (Test-Path -Path $Path))
    {
        $null = New-Item -Path $Path -ItemType Directory
    }
    if ($SetLocation)
    {
        Set-Location -Path $Path
    }
}

$EMACS_SEARCHPATH = 'C:\ProgramData','C:\Program Files','C:\Program Files (x86)', $env:USERPROFILE
$EMACS = Get-ChildItem -Path $EMACS_SEARCHPATH -Filter runemacs.exe -Recurse -ErrorAction SilentlyContinue | Select-Object -First 1
function emacs
{
    [CmdletBinding()]
    param()
    if ($EMACS)
    {
        & $EMACS -q -l "$PSScriptRoot\.emacs"
    }
    else
    {
        Write-Host "Unable to find runemacs.exe in the following paths: $($EMACS_SEARCHPATH -join ', ')"
    }
}
#endregion